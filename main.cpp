#include <string>
#include <map>

#include <ax_map_analyser.hpp>
#include <ax_map_generator.hpp>

template<int N = 10>
class Hystory {
private:
  int array_[N] = {0};
  int idx_ = 0;

public:
  int sum() {
    int res = 0;
    for (int i = 0; i < N; i++)
      res += array_[i];
    return res;
  }

  void push(int x) {
    array_[idx_] = x;
    idx_ = (idx_ + 1) % N;
  }
};

using namespace ax;

float mapD04(int x, int y) {
  int X = 200, Y = 200;
  float R_SAN = 70, R_AT = 90;
  float sigma = 0.4f;
  float r = r_points(Point(X / 2, Y / 2), Point(x, y));
  float type;

  if (r <= R_SAN) { // inside
    do {
      type = Get_Normal(r / R_SAN, sigma);
    } while (type < 0 || type > 1);
    return type;
  } else if (r <= R_AT) { // buffer zone
    float reverse_r = 1.f - (r - R_SAN) / (R_AT - R_SAN);
    if (fabsf(Get_Normal<float>(0, 0.7f)) > reverse_r)
      return mag::Cell_AT();
    do {
      type = Get_Normal<float>(1, sigma);
    } while (type < 0 || type > 1);
    return type;
  }
  return mag::Cell_AT();
}

int main(int argc, char *argv[]) {
  mag::Map tmap(200, 200, "map_200x200_dot5.map");
  tmap.Create(mapD04);
  //tmap.Filter(ax::FILTER_sharp);
  tmap.Write();

  man::Interpolant2 I2(0, 1, 0);
  printf("%f\n", I2(-1));
  printf("%f\n", I2.Reverse(0.75));

  man::Interpolant1 I1(0, 1);
  printf("%f\n", I1(-1));
  printf("%f\n", I1.Reverse(0.5));

  printf("\n");
  man::Interpolant3 I3(0, 1, -1, 0);
  printf("%f\n", I3(-2.f));
  printf("%f\n", I3(-1.5f));
  printf("%f\n", I3(-1.f));
  printf("%f\n", I3.Reverse(-0.5f));

  if (argc < 4)
    return 1;
  int X, Y;
  X = atoi(argv[1]);
  Y = atoi(argv[2]);

  man::Map<10> map(argv[3], X, Y);
  std::string oname(argv[3]);
  oname += ".out";
  FILE *fout = fopen(oname.c_str(), "w");
  int edge = 10;

  Hystory<20> hystory;
  auto STATE = man::PROCESS_STATE::RELAXING;
  std::list<std::pair<int, int>> stacked_centres; // (x, y)
  std::map<std::pair<int, int>, int> all_centres; // (x, y, times)
  std::vector<std::tuple<float, int, int>> all_centres_repeat; // (t, x, y)
  Point corner(30, 30);

  //std::list<std::pair<int, int>> V_points = {{60, 100}, {115, 100}, {100, 85}, {100, 140}};

  bool success = true;
  float last_T = 0;
  Moving_average T(0.5f);

  fprintf(fout, "Frame\tTime, s\tNumber of centres\tT, s\n");
  map.ClearOutFrame();
  float *tempOut = new float[X * Y];
  for (int i = 0; success; ++i) {
    if (i > 2000) break;
    decltype(stacked_centres) current_centres;

    /*/! V
    float V;
    for (auto &&p : V_points) {
      float g_mod;
      auto grad = map.getGradient(p.first, p.second, &g_mod);
      V = map.getWaveSpeed(p.first, p.second);
      if (V > 0)
        printf("\n%d (%d %d): grad = (%f %f), V = %f\n",
               map.getFrame(), p.first, p.second, grad.first, grad.second, V);
    }
    //*/

    //map.ClearOutFrame(); // LC
    for (int y = edge; y < Y - edge; ++y) {
      for (int x = edge; x < X - edge; ++x) {
        //! Leading centre
        if (map.isLeadingCentreNew(x, y)) {
          current_centres.emplace_back(x, y);
          all_centres_repeat.emplace_back(std::make_tuple(map.getTime(), x, y));
          map.Eout(x, y) += 1; // LC
        }
        //*/

        /*/! Grad
        float g;
        map.getGradient(x, y, &g);
        map.Eout(x, y) = g;
         //*/

        //if (map.isSpark(x, y)) map.Eout(x, y) = map.getTime(); // chronotopographic map

        /*/! Reentry
          map.Eout(x, y) = map.isSingularity(x, y);
        //*/

        //map.Eout(x, y) = map.getPhase(x, y); // Phase
      }
    }
    /*/! Grad
    for (int y = edge; y < Y - edge; ++y) {
      for (int x = edge; x < X - edge; ++x) {
        float g;
        auto grad = map.getGradient(x, y, &g);
        if (x % 7 == 0 && y % 7 == 0) {
          map.Write_Line_Out_Frame({x, y}, grad, {-80, 30}, (int)(g/2));
        }
      }
    }
    //*/
    //map.WriteOutFrame(); // Phase
    /*/! Reentry
    map.FilterOutFrame(man::FILTER_mean3x3);
    for (int y = edge; y < Y - edge; ++y) {
      for (int x = edge; x < X - edge; ++x) {
        if (map.Eout(x, y) > 0.95f) {
          fprintf(fout, "%d\t%.6f\t%d\t%d\t%.2f\n",
                  map.getFrame(), map.getTime(), x, y, map.Eout(x, y));
        }
      }
    }
    fflush(fout);
     */
    //map.WriteOutFrame();

    /*/! Interpolants test
    for(float t = -1.0f; t < 0.0f; t += 0.1f) {
      map.ClearOutFrame();
      for (int y = 0; y < Y; ++y) {
        for (int x = 0; x < X; ++x) {
          map.Eout(x, y) = map.interpols1_[map.xy(x, y)](t);
        }
      }
      map.WriteOutFrame();
    }
    //*/

    hystory.push(static_cast<int>(current_centres.size()));
    if (current_centres.size() > 0) {
      if (STATE == man::PROCESS_STATE::RELAXING) {
        T.update(map.getTime() - last_T);
        last_T = map.getTime();
      }
      STATE = man::PROCESS_STATE::IGNITION;
    }
    stacked_centres.splice(stacked_centres.end(), current_centres);

    if (STATE == man::PROCESS_STATE::IGNITION && hystory.sum() == 0) { // IGNITION -> RELAXING, drop stacked
      fprintf(fout, "%d\t%.6f\t%d\t%.4f\n",
              map.getFrame(),
              map.getTime(),
              static_cast<int>(stacked_centres.size()),
              T.get());
      fflush(fout);

      for (auto &&c : stacked_centres) {
        all_centres[c]++;
      }

      stacked_centres.clear();
      //all_centres_repeat.splice(all_centres_repeat.end(), stacked_centres);
      STATE = man::PROCESS_STATE::RELAXING;

      //! Leading centres
      { // Frame with density
        map.FilterOutFrame(ax::FILTER_mean3x3, 3);
        map.NormalizeOutFrame(100);
        //map.WriteOutFrame();
        map.Draw_Number_Out_Frame<100>(corner, map.getFrame());
        map.DumpOutFrame(tempOut);
        map.ClearOutFrame();
      } //*/
    }

    //! Leading centres
    if (i % 100 == 0) // every 0.5 s
      map.WriteOutFrame(tempOut);
    //*/

    if (map.getFrame() % 10 == 0)
      printf("|");
    if (map.getFrame() % 500 == 0)
      printf("\nframe: %d\n", map.getFrame());
    fflush(stdout);
    success = map.NextFrame();
  };

  double threshold = (map.getTime() / T.get()) * 0.01; // 1% of all ignitions
  fprintf(fout, "\n\n--------------------- %s ---------------------\n", "Stat: all centres with number (x, y, times)");
  fprintf(fout, "Threshold = %d\n", static_cast<int>(threshold));
  fprintf(fout, "X\tY\tNumber\n");
  // { plot fix
  fprintf(fout, "%d\t%d\t%d\n", 0, 0, 0);
  fprintf(fout, "%d\t%d\t%d\n", 0, Y, 0);
  fprintf(fout, "%d\t%d\t%d\n", X, 0, 0);
  fprintf(fout, "%d\t%d\t%d\n", X, Y, 0);
  // } fix
  std::list<std::pair<std::pair<int, int>, int>> list_centres;
  for (auto &&c : all_centres) {
    if (c.second < threshold)
      continue;
    list_centres.emplace_back(c.first, c.second);
  }
  list_centres.sort([](std::pair<std::pair<int, int>, int> &a, std::pair<std::pair<int, int>, int> &b) -> bool {
    return a.second > b.second;
  });

  map.ClearOutFrame();
  for (auto &&c : list_centres) {
    fprintf(fout, "%d\t%d\t%d\n", c.first.first, c.first.second, c.second);
    map.Eout(c.first.first, c.first.second) = c.second;
  }
  map.FilterOutFrame(ax::FILTER_mean3x3, 3);
  map.NormalizeOutFrame(100);
  map.WriteOutFrame();
  fprintf(fout, "\nMAP\t");
  for (int x = 0; x < X; ++x)
    fprintf(fout, "%d\t", x + 1);
  fprintf(fout, "\n");
  for (int y = 0; y < Y; ++y) {
    fprintf(fout, "%d\t", y + 1);
    for (int x = 0; x < X; ++x) {
      fprintf(fout, "%.2f\t", map.Eout(x, y));
    }
    fprintf(fout, "\n");
  }

  fprintf(fout, "\n\n--------------------- %s ---------------------\n", "Stat: all centres (x, y)");
  fprintf(fout, "Time, s\tX\tY\n");
  // { plot fix
  fprintf(fout, "0\t%d\t%d\n", 0, 0);
  fprintf(fout, "0\t%d\t%d\n", 0, Y);
  fprintf(fout, "0\t%d\t%d\n", X, 0);
  fprintf(fout, "0\t%d\t%d\n", X, Y);
  // } fix

  // (t, R_mean, R_sigma)
  std::list<std::tuple<float, float, float, float>> group_centres;
  std::list<float> current_Rs;

  float current_t = std::get<0>(all_centres_repeat.front());

  for (auto &&c : all_centres_repeat) {
    fprintf(fout, "%.3f\t%d\t%d\n", std::get<0>(c), std::get<1>(c), std::get<2>(c));
    if (std::get<0>(c) - current_t > 0.1f) { // calculate and drop
      auto M_D_SD = Get_M_D_SD(current_Rs);
      group_centres.emplace_back(current_t,
                                 std::get<0>(M_D_SD),
                                 std::get<1>(M_D_SD),
                                 std::get<2>(M_D_SD));

      current_t = std::get<0>(c);
      current_Rs.clear();
    }
    current_Rs.emplace_back(r_points(Point(100, 100), Point(std::get<1>(c), std::get<2>(c))));
  }

  fprintf(fout, "\nTime, s\tR mean\tR dispersion\tR SD\n");
  for (auto &&c : group_centres) {
    fprintf(fout, "%.3f\t%.3f\t%.3f\t%.3f\n",
            std::get<0>(c), std::get<1>(c), std::get<2>(c), std::get<3>(c));
  }

  fclose(fout);
  return 0;
}
